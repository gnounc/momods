#!/usr/bin/env python3

import os
import sys
import json
import shutil
import urllib
import platform
import pathlib
import subprocess
import webbrowser
import pkg_resources
import urllib.request
from threading import Timer
from zipfile import ZipFile


def clearConsole():
	if os.name == 'nt':
		os.system('cls')
 
	else:
		os.system('clear')

#prompt to install pyqt5 dependancy
try:
	from PyQt5 import QtGui
	from PyQt5 import QtCore
	from PyQt5.QtWidgets import * 
	from PyQt5.QtGui import QIcon
	from PyQt5.QtWidgets import QMenu
	from PyQt5.QtGui import QKeySequence

except ImportError:

	clearConsole()

	while True:

		response = str(input("\nPyQt5 required.\nInstall python module PyQt5? (y/n): ")).lower().strip()
		if response[:1] == "y":
			print("Installing dependancy PyQt5")
			python = sys.executable
			subprocess.check_call([python, '-m', 'pip', 'install', "PyQt5"], stdout = subprocess.DEVNULL)

			clearConsole()

			print("Installing complete, please re-run application.")
			exit()

		if response[:1] == "n":

			clearConsole()

			print("Installation declined. Exiting.")
			exit()


#pin the search row to the top.
class TableItem(QTableWidgetItem):
	def __lt__(self, other):

		order = self.tableWidget().horizontalHeader().sortIndicatorOrder()

		if order == QtCore.Qt.AscendingOrder:
			if self.text() == "--":
				return True

			if other.text() == "--":
				return False

		if order == QtCore.Qt.DescendingOrder:
			if self.text() == "--":
				return False

			if other.text() == "--":
				return True

		return super(TableItem, self).__lt__(other)


class About(QDialog):
	def __init__(self, parent):
		super(About, self).__init__(parent)
		
		self._parent = parent
		self.setWindowTitle("About")
		self.setGeometry(0, 0, 200, 100)

		vbox = QVBoxLayout()
		hbox = QHBoxLayout()

		hbox.addStretch()
		hbox.addWidget(QLabel("Copyright © 2023 gnounc."))
		hbox.addStretch()

		vbox.addStretch()
		vbox.addLayout(hbox)
		vbox.addStretch()


		self.setLayout(vbox)
		self.show()

class Settings(QDialog):
	def __init__(self, parent):
		super(Settings, self).__init__(parent)
		
		self._parent = parent
		self.setWindowTitle("Settings")
		self.setGeometry(0, 0, 400, 200)

		self.checkboxes = []

		layout = QVBoxLayout()
		top = QHBoxLayout()
		left = QVBoxLayout()
		right = QVBoxLayout()


#input boxes
		self.cfgInput = self.createFolderSettings(left, "openmw.cfg File:", self._parent.configFolder)
		self.downloadInput = self.createFolderSettings(left, "Download Folder:", self._parent.downloadFolder)
		self.modInput = self.createFolderSettings(left, "Mod Folder:", self._parent.modFolder)
		self.backupInput = self.createFolderSettings(left, "Backup Folder:", self._parent.backupFolder)

#search columns
		columnsLabel = QLabel("Show Columns:")
		right.addWidget(columnsLabel)

		for column in self._parent.show_columns:
			checkbox = QCheckBox(column)
			checkbox.setChecked(self._parent.show_columns[column])
			self.checkboxes.append(checkbox)

#			commented out because windows cant behave itself.
#			if column == "⇩":	#dont show the download column. we are going to hardcode this to always appear
#				checkbox.setEnabled(False)

			right.addWidget(checkbox)

		top.addLayout(left)
		top.addLayout(right)

		buttonArea = QHBoxLayout()
		buttonArea.addStretch()


		cancelButton = QPushButton("Cancel")
		cancelButton.clicked.connect(self.cb_settings_cancel)
		cancelButton.setShortcut(QKeySequence("Esc"))
		buttonArea.addWidget(cancelButton)

		OkButton = QPushButton("Ok")
		OkButton.clicked.connect(self.cb_settings_apply)
		OkButton.setShortcut(QKeySequence("Return"))
		buttonArea.addWidget(OkButton)

		layout.addLayout(top)
		layout.addLayout(buttonArea)
		self.setLayout(layout)
		self.show()

#https://stackoverflow.com/questions/17317219/is-there-an-platform-independent-equivalent-of-os-startfile
	def openFolder(self, filename):
		if filename == None or filename == "":
			return

		if sys.platform == "win32":
			os.startfile(filename)
		else:
			opener = "open" if sys.platform == "darwin" else "xdg-open"
			subprocess.call([opener, filename])


	def createFolderSettings(self, parentLayout, label, folderPath):
		layout = QVBoxLayout()

		inputLayout = QHBoxLayout()
		folderButton = QPushButton("📁", clicked=lambda: self.openFolder(folderPath))
		folderButton.setFixedWidth(40)
#		folderButton.clicked.connect(self.cb_open_folder)

		inputLabel = QLabel(label)
		inputBox = QLineEdit(self)
		inputBox.setFixedWidth(200)

		if folderPath != None:
			inputBox.setText(folderPath)

		inputLayout.addWidget(inputBox)
		inputLayout.addWidget(folderButton)

		layout.addWidget(inputLabel)
		layout.addLayout(inputLayout)

		parentLayout.addLayout(layout)

		return inputBox


	def cb_setColumnVisibility(self):
		for checkbox in self.checkboxes:
			self._parent.show_columns[checkbox.text()] = checkbox.isChecked()
			self._parent.setColumnVisibility()

#prevent settings button from opening multiple settings windows
	def closeEvent(self, unknown):
		self._parent.settingsOpen = False

	def cb_settings_apply(self):
		self.cb_setColumnVisibility()
		self._parent.modFolder = self.modInput.text()
		self._parent.configFolder = self.cfgInput.text()
		self._parent.downloadFolder = self.downloadInput.text()
		self._parent.backupFolder = self.backupInput.text()

		self.close()

	def cb_settings_cancel(self):
		self.close()



#Main Window
class App(QWidget):
	def __init__(self):
		super().__init__()

		self.setWindowTitle("Momods Moprobs")
		self.setWindowIcon(QIcon("gfx/icon.png"))
		self.setGeometry(0, 0, 800, 400)

		self.searchRow = None

		self.settingsOpen = False

		#these hold the strings we're going to jam in openmw.cfg
		self.modData = []
		self.modContent = []

		self.modFolder= None
		self.configFolder = None
		self.backupFolder = None
		self.downloadFolder = None

		self.configFile = None
		self.installedData = []
		self.installedContent = []

		self.downloadQueue = []
		self.concurrentDownloads = 0


		self.layout = QVBoxLayout()
		self.header = QHBoxLayout()
		self.footer = QHBoxLayout()

		paymentOptions = QHBoxLayout()
		groupbox = QGroupBox("Donations Welcome")
		groupbox.setCheckable(False)
		groupbox.setFlat(True);
		groupbox.setLayout(paymentOptions)

#main menu
		mainMenu = self.createMainMenu()		
		self.layout.setMenuBar(mainMenu)

		#header
		self.header.addStretch()

		clearButton = QPushButton("Clear Filters")
		clearButton.clicked.connect(self.cb_clear_search)
		self.header.addWidget(clearButton)

		settingsButton = QPushButton("⚙")
		settingsButton.clicked.connect(self.cb_settings_menu)
		self.header.addWidget(settingsButton)

		#table
		self.createTable()

#payment options
		patreonButton = QPushButton("Patreon")
		patreonButton.clicked.connect(self.cb_visit_site)
		patreonButton.setIcon(QIcon("gfx/Digital-Patreon-Logo_FieryCoral.png"))
		paymentOptions.addWidget(patreonButton)


#		liberapay currently does not allow private donations.
#		liberaPayButton = QPushButton("LiberaPay")
#		liberaPayButton.clicked.connect(self.cb_visit_site)
#		liberaPayButton.setIcon(QIcon("gfx/icon-v2_white-on-yellow2.png"))
#		paymentOptions.addWidget(liberaPayButton)

		#footer
		self.footer.addWidget(groupbox)
		self.footer.addStretch()
		downloadButton = QPushButton("⇩")
		downloadButton.clicked.connect(self.cb_download)
		self.footer.addWidget(downloadButton)


		self.layout.addLayout(self.header)
		self.layout.addWidget(self.tableWidget)
		self.layout.addLayout(self.footer)
		self.setLayout(self.layout)

		#load settings
		self.loadSettings()
		self.setColumnVisibility()


		#Show window
		self.show()

		conf = self.findConfig()
		print(str(conf))


	def cb_visit_site(self):
		site = self.sender().text() #object that sent the event

		if site == "Patreon":
			webbrowser.open_new_tab("https://www.patreon.com/gnounc")

		if site == "LiberaPay":
			webbrowser.open_new_tab("https://liberapay.com/gnounc/")

#			urllib.request.urlopen("https://liberapay.com/gnounc/")
				##useful for when i want to download the moddlist.


	def getInstalledMods(self):
		with open(self.findConfig(), 'r') as config:
			for line in config.readlines():
				if line[0:6] == "data=\"":
					self.installedData.append(line[6:].rstrip("\n").rstrip("\""))
				if line[0:8] == "content=":
					self.installedContent.append(line[8:].rstrip("\n").rstrip("\""))


	def loadSettings(self):
		print("\n\nnow loading settings. ")

		settings = QtCore.QSettings("gnounc", "Momods Moprobs");

		self.modFolder = settings.value("modFolder")
		self.configFolder = settings.value("configFolder")
		self.backupFolder = settings.value("backupFolder")
		self.downloadFolder = settings.value("downloadFolder")

		for key, value in self.show_columns.items():
			val = settings.value("show_column_" + key, type = bool)
			if val == None:
				val = True
			if val == "⇩":
				val = True

			self.show_columns[key] = val


	def saveSettings(self):
		print("\n\nnow saving settings. ")
		settings = QtCore.QSettings("gnounc", "Momods Moprobs");

		settings.setValue("modFolder", self.modFolder);
		settings.setValue("configFolder", self.configFolder);
		settings.setValue("backupFolder", self.backupFolder);
		settings.setValue("downloadFolder", self.downloadFolder);

		for key, value in self.show_columns.items():
			settings.setValue("show_column_" + key, value);


	def closeEvent(self, event):
		self.saveSettings()

	def findConfig(self):

		#try user supplied path first
		if self.configFolder != None:
			path = pathlib.Path(self.configFolder)

			if path.exists() and path.name == "openmw.cfg":
				return path

		#TODO: make sure its not different for xp/7 or 11
		os = platform.system()
		if os == "Windows":
			path = pathlib.Path.home().joinpath("Documents/My Games/OpenMw/openmw.cfg")
			if path.exists():
				return path
		if os == "Darwin":
			path = pathlib.Path.home().joinpath("Library/Preferences/openmw/openmw.cfg")
			if path.exists():
				return path
		#find out what other common paths are.
		if "Linux" in os:
			path = pathlib.Path.home().joinpath(".config/openmw/openmw.cfg")
			if path.exists():
				return path

		return path.resolve()


	def writeConfig(self):
		#populate self.installedData and self.installedContent
		self.getInstalledMods()

		with open(self.findConfig(), 'a') as config:
			config.write("\n")

			for data in self.modData:
				if data in self.installedData:
					print(data + " already installed. skipping.")
				else:
					config.write("data=\"" + data + "\"\n")

			for content in self.modContent:
				if content in self.installedContent:
					print(content + " already installed. skipping.")
				else:
					config.write("content=" + content + "\n")

		print("writing to config file: " + str(self.findConfig()))



	def findScriptOrAddon(self, folderPath):
		filestypes = ("*.omwscripts", "*.omwaddon", "*.esp", "*.esm")

		found = []

		for ftype in filestypes:
			for path in pathlib.Path(folderPath).rglob(ftype):
				found.append(path)

		return found


	def getProjectName(self, repo):
		tokens = repo.split('/')
		return tokens[-1]

	#the filename github downloads does not match the link. thx github.
	def getDownloadFilename(self, repo):
		filename = repo.split("/")[-1]
		print("getDownloadFilename: " + filename)
		return filename

	#in the great purge, all references to master were replaced with main. so now we have broken links.
	def getDownloadLink_Master(self, repo):
		if "gitlab" in repo:
			return repo + "/-/archive/master/" + self.getProjectName(repo) + "-master.zip"

		if "github" in repo:
			return  repo + "/archive/refs/heads/master.zip" 


	def getDownloadLink(self, repo):
		if "gitlab" in repo:
			return repo + "/-/archive/main/" + self.getProjectName(repo) + "-main.zip"

		if "github" in repo:
			return repo + "/archive/refs/heads/main.zip"


	def addItem(self, item):

		self.tableWidget.insertRow(0)
		columnNames = list(self.show_columns.keys())

		for colIdx in range(len(self.show_columns)):
			columnName = columnNames[colIdx].title()

			if columnName in item:
				self.tableWidget.setItem(0, colIdx, TableItem(item[columnName]))
			else:
				self.tableWidget.setItem(0, colIdx, TableItem(""))
				print("column " + columnName + " not in json. ")



	def setColumnVisibility(self):
		header = self.tableWidget.horizontalHeader()
		for colIdx in range(len(header)):
			headerName = self.tableWidget.horizontalHeaderItem(colIdx).text()
			self.tableWidget.setColumnHidden(colIdx, not self.show_columns[headerName])


	def getCheckedRepos(self):
		colCount = len(self.tableWidget.horizontalHeader())

		try:
			repoColumnIndex = list(self.show_columns.keys()).index("Repo")
			downloadColumnIndex = list(self.show_columns.keys()).index("⇩")

		except ValueError:
			print("No 'Repo' key in table.(and thus in json file. returning 0 downloads.)")
			return

		for row in range(self.tableWidget.rowCount()):
			checkbox = self.tableWidget.cellWidget(row, colCount - 1)
			if checkbox and isinstance(checkbox, QCheckBox): #get download cell
				if checkbox.isChecked():
					repoName = self.tableWidget.item(row, repoColumnIndex).text()
					filename = self.getDownloadFilename(repoName)
					downloadLink = self.getDownloadLink(repoName)

					#collect information required by downloadfile() function.
					self.downloadQueue.append({"rowIdx": row, "repo": repoName, "link": downloadLink, "filename": filename.rstrip("") })

					#replace download checkbox with "Downloading" notification
					self.tableWidget.setCellWidget(row, downloadColumnIndex, QLabel("Downloading"))



	def unzipFile(self, file, extractTo):
		with ZipFile(file, 'r') as zip_ref:

			for i, f in enumerate(zip_ref.filelist):
				f.filename = f.filename.replace("-main", "").replace("-master", "")	#nuke obnoxious main/master from filename so i dont have to do any guessing to extract. and because i hate it.
				zip_ref.extract(f, path = extractTo)


	def getUserFoldersOrDefault(self):
		folders = {"downloads": self.downloadFolder, "mods": self.modFolder, "backup": self.backupFolder}

		for key, value in folders.items():
			default = pathlib.Path("./", key)

			if value == None or value == "":
				path = default
			else:
				path = pathlib.Path(value)

			if not path.exists():
				path = default
				print("provided " + key + " folder did not exist. using default folder.")

			if not path.exists():
				pathlib.Path.mkdir(path)
				print("folder did not exist. creating folder: " + str(path))

			folders[key] = path.resolve()

		return folders

	def downloadFile(self):

		if len(self.downloadQueue) < 1:
			return

		self.concurrentDownloads = self.concurrentDownloads + 1

		download = self.downloadQueue.pop()
		link = download["link"]
		rowIdx = download["rowIdx"]
		colIdx = len(self.show_columns) - 1

		filename = download["filename"]
		folders = self.getUserFoldersOrDefault()


		#older git download links use "master". newer ones use "main". so we guess, we check, then we guess again.
		try:
			urllib.request.urlretrieve(link, folders["downloads"].joinpath(filename).resolve())
		except:
			try:
				link = self.getDownloadLink_Master(download["repo"])
				urllib.request.urlretrieve(link, folders["downloads"].joinpath(filename).resolve())
			
			except:
				self.concurrentDownloads = self.concurrentDownloads - 1
				self.tableWidget.setCellWidget(rowIdx, colIdx, QLabel("Failed"))
				print("unknown error during download.")
				return

		print ("Download Complete")

		#let user know the files are extracting
		self.tableWidget.setCellWidget(rowIdx, colIdx, QLabel("Extracting"))

		self.unzipFile(folders["downloads"].joinpath(filename), folders["mods"])
		print ("Extraction Complete")

		#let user know download is finished
		self.tableWidget.setCellWidget(rowIdx, colIdx, QLabel("Complete"))

		#update modlist for updating openmw.cfg
		modfldr = folders["mods"].joinpath(pathlib.Path(filename).stem).resolve()
		scripts = self.findScriptOrAddon(modfldr)

		script = str(scripts[0].parent)
		if script not in self.modData:
			self.modData.append(script)

		for path in scripts:
			if path.name not in self.modContent:
				self.modContent.append(path.name)

		self.concurrentDownloads = self.concurrentDownloads - 1


		#everthing after this line should be done after the downloads complete
		if self.concurrentDownloads > 0:
			return

		#spit out config file when downloads are finished
		backupFile = folders["backup"].joinpath("openmw.cfg")
		if not backupFile.exists():
			print("backing up config file: " + str(self.findConfig()))
			shutil.copy2(self.findConfig(), folders["backup"])
		else:
			print("config backup " + str(backupFile.resolve()) + " exists. no additional backup will be made.")

		#write to config file
		self.writeConfig()


	#kick this off in a separate thread
	def kickoffDownload(self):

		while len(self.downloadQueue) > 0:
			if self.concurrentDownloads < 4:
				self.downloadFile()


	def cb_download(self):
		self.getCheckedRepos()	#populates self.downloadQueue and sets the checkboxes to progress bars
		self.kickoffDownload()
		


	def cb_clear_search(self):

		for colIdx in range(len(self.show_columns) - 1):	#dont attempt to clear download cell. its not a text field
			searchbox = self.tableWidget.cellWidget(0, colIdx)
			searchbox.setText("")


	def cb_settings_menu(self):
		if self.settingsOpen == True:
			return

		self.settingsOpen = True
		self.dialog = Settings(self)


	def getSearchString(self, columnName):

		for colIdx in range(len(self.show_columns) - 1):	#dont add entry box to download cell. its not a text field
			header = self.tableWidget.horizontalHeaderItem(colIdx)

			if header.text() == columnName:
				searchbox = self.tableWidget.cellWidget(0, colIdx)
				return searchbox.text()


	def filterHeader(self, text):
		sender = self.sender() #object that sent the event
		column_search = {}


		for columnName, visible in self.show_columns.items():
			if visible:
				searchCriteria = self.getSearchString(columnName)
				if searchCriteria == None:
					searchCriteria = ""
				column_search[columnName] = searchCriteria


		for rowIdx in range(self.tableWidget.rowCount()):
			if rowIdx == 0:	#please dont nuke search row
				continue

			colIdx = 0
			hide = False

			for colIdx in range(self.tableWidget.columnCount()):
				headerName = self.tableWidget.horizontalHeaderItem(colIdx).text()

				#oh windows, y u so weird?
				if headerName in column_search.keys():
					search_string = column_search[headerName]
				else:
					search_string = ""

				if search_string == None or search_string == "":
					continue

				item = self.tableWidget.item(rowIdx, colIdx)
				if item == None:
					continue

				if search_string.lower() not in item.text().lower():
					hide = True

			self.tableWidget.setRowHidden(rowIdx, hide)


	def cb_AboutMenu(self):
		self.dialog = About(self)


	def createMainMenu(self):
		mainMenu = QMenuBar(self)

		fileMenu = mainMenu.addMenu("&File")
		exitAction = QAction("&Quit", self)
		exitAction.triggered.connect(self.close)
		exitAction.setShortcut(QKeySequence("Ctrl+Q"))
		fileMenu.addAction(exitAction)

		helpMenu = mainMenu.addMenu("&Help")
		aboutAction = QAction("&About", self)
		aboutAction.triggered.connect(self.cb_AboutMenu)
		helpMenu.addAction(aboutAction)
		


	#Create table
	def createTable(self):
		self.tableWidget = QTableWidget()
		self.show_columns = {}


		#Column count
		self.tableWidget.setColumnCount(len(self.show_columns))
		self.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)
		self.tableWidget.setSortingEnabled(True)
		self.tableWidget.setAlternatingRowColors(True)

		self.populateTable()
		self.addSearchRow()

		self.tableWidget.horizontalHeader().setStretchLastSection(True)
		self.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
#		self.tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.ResizeToContents) 

		self.tableWidget.sortItems(1, order = QtCore.Qt.AscendingOrder)
		self.setColumnVisibility()


	def addSearchRow(self):

		self.tableWidget.insertRow(0)

		columnNames = list(self.show_columns.keys())
		for colIdx in range(len(columnNames)):	#dont add entry box to download cell. its not a text field

			columnName = columnNames[colIdx]
			if columnName != "⇩":		#dont add search to download column
				e = QLineEdit(self)
				e.setPlaceholderText("Search " + columnNames[colIdx])
				e.textChanged.connect(self.filterHeader)
				self.tableWidget.setItem(0, colIdx,  TableItem("--"))
				self.tableWidget.setCellWidget(0, colIdx, e)


	def loadModlistFiles(self):
		modlists = []

		for path in pathlib.Path(pathlib.Path.cwd()).rglob("*.modlist"):
			with open(str(path), 'r') as f_modlist:
				modlists.append(json.load(f_modlist))

		return modlists

	def addDownloadColumn(self):

		self.insertColumn("⇩")
		self.tableWidget.setHorizontalHeaderLabels(self.show_columns.keys());

		downloadColumnIndex = list(self.show_columns.keys()).index("⇩")

		for rowIdx in range(self.tableWidget.rowCount()):
			checkbox = QCheckBox("")
			self.tableWidget.setCellWidget(rowIdx, downloadColumnIndex, checkbox)



	def insertColumn(self, new_column):
		print("column " + new_column + " not in table. adding column.")
		self.tableWidget.insertColumn(len(self.show_columns))
		self.show_columns[new_column] = True


	def populateTable(self):
		modlists = self.loadModlistFiles()

		#populate table
		for modlist in modlists:
			firstItem = modlist[0]

			print(firstItem.keys())

			for key in firstItem.keys():
				print("key: " + key)
				if key not in self.show_columns.keys():
					self.insertColumn(key)

			for mod in modlist:
				self.addItem(mod)

		self.addDownloadColumn()



if __name__ == '__main__':

	app = QApplication(sys.argv)
	ex = App()
	sys.exit(app.exec_())

