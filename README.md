# MoMods Moprobs

## Requires OpenMW

## Youtube Demonstration
https://www.youtube.com/watch?v=DdgfzfuS2Iw

## Installation
--todo

## Warning!:
1. This application will overwrite your openmw.cfg file. It will back the file up before it does so, but I wanted you to be aware.
2. This application will prompt you to install a python module for Qt, it will install the module if you accept, I've been
told that some people may not like this, so here (as well as the install prompt) is your warning.
3. I have tested the application as much as I can during development, but its hot off the presses, there surely are bugs.

## To Use:
0. Set up your folders in settings, and manually create the folders, or they will not be used. (optional)
1. Check mods you wish to download
2. Hit download button
3. Profit!!
	
## Known Bugs:
Download column does not appear in windows yet.

## License
--TODO: say how it is licensed.

